<?php

namespace Drupal\tfa_vault_totp\Plugin\TfaValidation;

use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\encrypt\EncryptionProfileManagerInterface;
use Drupal\encrypt\EncryptServiceInterface;
use Drupal\tfa\Plugin\TfaBasePlugin;
use Drupal\tfa\Plugin\TfaValidationInterface;
use Drupal\user\UserDataInterface;
use Drupal\vault\VaultClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Vault\Exceptions\RequestException;

/**
 * TOTP validation class for performing TOTP validation using the Vault TOTP Secret Engine.
 *
 * @TfaValidation(
 *   id = "tfa_vault_totp",
 *   label = @Translation("TFA Time-based one-time password (TOTP) Vault"),
 *   description = @Translation("TFA TOTP Validation Plugin using Vault's OTP Secret Engine"),
 *   setupPluginId = "tfa_vault_totp_setup",
 * )
 */
class TfaVaultTotpValidation extends TfaBasePlugin implements TfaValidationInterface, ContainerFactoryPluginInterface {
  use StringTranslationTrait;

  /**
   * The time-window in which the validation should be done.
   *
   * @var int
   */
  protected int $timeSkew;

  /**
   * Should the prefix use the site name.
   *
   * @var bool
   */
  protected bool $siteNamePrefix;

  /**
   * Name prefix.
   *
   * @var string
   */
  protected string $namePrefix;

  /**
   * Configurable name of the issuer.
   *
   * @var string
   */
  protected string $issuer;

  /**
   * @var \Drupal\vault\VaultClientInterface
   */
  protected VaultClientInterface $vaultClient;

  protected string $tokenPath;
  protected int $tokenPeriod;
  protected int $keySize;
  protected string $tokenAlgorithm;
  protected int $qrSize = 200;

  /**
   * Constructs a new Tfa plugin object.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin id.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\user\UserDataInterface $user_data
   *   User data object to store user specific information.
   * @param \Drupal\encrypt\EncryptionProfileManagerInterface $encryption_profile_manager
   *   Encryption profile manager.
   * @param \Drupal\encrypt\EncryptServiceInterface $encrypt_service
   *   Encryption service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The 'config.factory' service.
   * @param \Drupal\vault\VaultClientInterface $vault_client
   *   A vault client.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, UserDataInterface $user_data, EncryptionProfileManagerInterface $encryption_profile_manager, EncryptServiceInterface $encrypt_service, TranslationInterface $translation, ConfigFactoryInterface $config_factory, VaultClientInterface $vault_client) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $user_data, $encryption_profile_manager, $encrypt_service);
    $this->alreadyAccepted = FALSE;
    $this->setStringTranslation($translation);
    $this->vaultClient = $vault_client;
    $plugin_settings = $config_factory->get('tfa.settings')->get('validation_plugin_settings');
    $settings = [];
    if (is_array($plugin_settings) && array_key_exists('tfa_vault_totp', $plugin_settings) && is_array($plugin_settings['tfa_vault_totp'])) {
      $settings = $plugin_settings['tfa_vault_totp'];
    }

    $settings = array_replace([
      'time_skew' => 1,
      'site_name_prefix' => TRUE,
      'name_prefix' => 'TFA',
      'issuer' => 'Drupal',
      'token_path' => 'totp',
      'key_size' => 20,
      'period' => 30,
      'algorithm' => "SHA1",
      'code_length' => 6,
    ], $settings);

    $this->timeSkew = (int) $settings['time_skew'];
    $this->tokenPath = $settings['token_path'];
    $this->keySize = (int) $settings['key_size'];
    $this->tokenPeriod = (int) $settings['period'];
    $this->tokenAlgorithm = $settings['algorithm'];
    $this->siteNamePrefix = $settings['site_name_prefix'];
    $this->namePrefix = $settings['name_prefix'];
    $this->issuer = $settings['issuer'];
    $this->codeLength = (int) $settings['code_length'];
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('user.data'),
      $container->get('encrypt.encryption_profile.manager'),
      $container->get('encryption'),
      $container->get('string_translation'),
      $container->get('config.factory'),
      $container->get('vault.vault_client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function ready(): bool {
    $vault_path = sprintf('/%s/keys/%s', $this->tokenPath, $this->getTokenName());

    try {
      $response = $this->vaultClient->read($vault_path);
      return TRUE;
    }
    catch (RequestException $e) {
      if ($e->getCode() == 404) {
        // Only when the vault responds that the token doesn't exist should we
        // consider the user not provisioned.
        return FALSE;
      }
    }
    // Exceptions are not on the API specs, however it is the only way to indicate system failure.
    throw new \Exception('Unknown fault validating provisioning state');
  }

  /**
   * {@inheritdoc}
   */
  public function getForm(array $form, FormStateInterface $form_state): array {
    $message = $this->t('Verification code is application generated and @length digits long.', ['@length' => $this->codeLength]);
    if ($this->getUserData('tfa', 'tfa_recovery_code', $this->uid, $this->userData)) {
      $message .= '<br/>' . $this->t("Can't access your account? Use one of your recovery codes.");
    }
    $form['code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Application verification code'),
      '#description' => $message,
      '#required'  => TRUE,
      '#attributes' => [
        'autocomplete' => 'off',
        'autofocus' => 'autofocus',
      ],
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['login'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->t('Verify'),
    ];

    return $form;
  }

  /**
   * The configuration form for this validation plugin.
   *
   * @param \Drupal\Core\Config\Config $config
   *   Config object for tfa settings.
   * @param array $state
   *   Form state array determines if this form should be shown.
   *
   * @return array
   *   Form array specific for this validation plugin.
   */
  public function buildConfigurationForm(Config $config, array $state = []): array {
    if (empty($state)) {
      $state['visible'] = [];
    }

    $settings_form['token_path'] = [
      '#type' => 'select',
      '#title' => $this->t('Token path'),
      '#options' => [],
      '#default_value' => $this->tokenPath,
      '#description' => $this->t('Secret engine path to store tokens. <strong>Warning</strong>: Changing this will invalidate any tokens that have already been configured permitting login without presenting a token'),
      '#states' => $state,
      '#required' => TRUE,
    ];

    $mount_points = $this->vaultClient->listSecretEngineMounts(['totp']);

    foreach ($mount_points as $mount => $info) {
      $settings_form['token_path']['#options'][trim($mount, '/')] = $mount;
    }

    $settings_form['header_1'] = [
      '#type' => 'item',
      '#title' => $this->t('All options below impact new tokens only'),
    ];

    $settings_form['code_length'] = [
      '#type' => 'select',
      '#title' => $this->t('Token Length'),
      '#options' => [
        6 => 6,
        8 => 8,
      ],
      '#default_value' => $this->codeLength,
      '#description' => $this->t('Number of digits for token.'),
      '#states' => $state,
      '#required' => TRUE,
    ];

    $settings_form['time_skew'] = [
      '#type' => 'select',
      '#title' => $this->t('Time Skew'),
      '#options' => [
        0 => '0',
        1 => '1',
      ],
      '#default_value' => $this->timeSkew,
      '#description' => $this->t('Number of past codes to consider valid. This setting only impacts new tokens.'),
      '#states' => $state,
      '#required' => TRUE,
    ];

    $settings_form['site_name_prefix'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use site name as OTP QR code name prefix.'),
      '#default_value' => $this->siteNamePrefix,
      '#description' => $this->t('If checked, the site name will be used instead of a static string. This can be useful for multi-site installations.'),
      '#states' => $state,
    ];

    // Hide custom name prefix when site name prefix is selected.
    $prefix_state = $state;
    $prefix_state['visible'] += [
      ':input[name="validation_plugin_settings[tfa_vault_totp][site_name_prefix]"]' => ['checked' => FALSE],
    ];

    $settings_form['name_prefix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OTP QR Code Prefix'),
      '#default_value' => $this->namePrefix ?? 'tfa',
      '#description' => $this->t('Prefix for OTP QR code names. Suffix is account username.'),
      '#size' => 15,
      '#states' => $prefix_state,
    ];

    $settings_form['issuer'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Issuer'),
      '#default_value' => $this->issuer,
      '#description' => $this->t('The provider or service this account is associated with.'),
      '#size' => 15,
      '#required' => TRUE,
    ];

    $settings_form['period'] = [
      '#type' => 'number',
      '#title' => $this->t('Period'),
      '#default_value' => $this->tokenPeriod,
      '#description' => $this->t('How frequently tokens should rotate.'),
      '#states' => $state,
      '#required' => TRUE,
    ];

    $settings_form['algorithm'] = [
      '#type' => 'select',
      '#title' => $this->t('Algorithm'),
      '#options' => [
        'SHA1' => $this->t('SHA1 (most common)'),
        'SHA256' => 'SHA256',
        'SHA512' => 'SHA512',
      ],
      '#default_value' => $this->tokenAlgorithm,
      '#states' => $state,
      '#required' => TRUE,
    ];

    $settings_form['key_size'] = [
      '#type' => 'number',
      '#title' => $this->t('Seed Length'),
      '#default_value' => $this->keySize,
      '#description' => $this->t('Length of the cryptographic seed used to generate tokens.'),
      '#states' => $state,
      '#required' => TRUE,
    ];

    return $settings_form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array $form, FormStateInterface $form_state): bool {
    $values = $form_state->getValues();
    if ($this->validate($values['code'])) {
      return TRUE;
    }
    $this->errorMessages['code'] = $this->t('Invalid application code. Please try again.');
    if ($this->alreadyAccepted) {
      $form_state->clearErrors();
      $this->errorMessages['code'] = $this->t('Invalid code, it was recently used for a login. Please try a new code.');
    }
    return FALSE;
  }

  /**
   * Simple validate for web services.
   *
   * @param int $code
   *   OTP Code.
   *
   * @return bool
   *   True if validation was successful otherwise false.
   */
  public function validateRequest($code): bool {
    return $this->validate(strval($code));
  }

  /**
   * {@inheritdoc}
   */
  protected function validate($code): bool {
    $this->alreadyAccepted = FALSE;
    $this->isValid = FALSE;
    // Strip whitespace.
    $code = preg_replace('/\s+/', '', $code);
    $vault_path = sprintf('/%s/code/%s', $this->tokenPath, $this->getTokenName());

    try {
      $response = $this->vaultClient->write($vault_path, ['code' => $code]);
    }
    catch (RequestException $e) {
      if ($e->getCode() == 400) {
        $this->alreadyAccepted = TRUE;
      }
      return FALSE;
    }
    /** @var array{'valid': bool}|null $response_data */
    $response_data = $response->getData();
    if ($response_data) {
      $this->isValid = $response_data['valid'];
      return $this->isValid;
    }
    return FALSE;
  }

  /**
   * Get name of token key for vault storage.
   *
   * @return string
   *   The name of the token as should be stored in Vault.
   */
  protected function getTokenName(): string {
    return sprintf('drupal-vault-totp-%s', $this->uid);
  }

  /**
   * Was this code previously accepted.
   *
   * @return bool
   *   Was the code previously accepted..
   */
  public function isAlreadyAccepted(): bool {
    return $this->alreadyAccepted;
  }

}
