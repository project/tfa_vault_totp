<?php

namespace Drupal\tfa_vault_totp\Plugin\TfaSetup;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\tfa_vault_totp\Plugin\TfaValidation\TfaVaultTotpValidation;
use Drupal\vault\VaultClientInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\encrypt\EncryptionProfileManagerInterface;
use Drupal\encrypt\EncryptServiceInterface;
use Drupal\tfa\Plugin\TfaSetupInterface;
use Drupal\user\UserDataInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Vault\Exceptions\RequestException;

/**
 * TOTP setup class to setup TOTP validation.
 *
 * @TfaSetup(
 *   id = "tfa_vault_totp_setup",
 *   label = @Translation("TFA Vault TOTP Setup"),
 *   description = @Translation("TFA Vault TOTP Setup Plugin"),
 *   helpLinks = {
 *    "Google Authenticator (Android/iOS)" = "https://googleauthenticator.net",
 *    "Microsoft Authenticator (Android/iOS)" = "https://www.microsoft.com/en-us/security/mobile-authenticator-app",
 *    "Authy (Android/iOS/Desktop)" = "https://authy.com",
 *    "FreeOTP (Android/iOS)" = "https://freeotp.github.io",
 *    "GAuth Authenticator (Desktop)" = "https://github.com/gbraadnl/gauth"
 *   },
 *   setupMessages = {
 *    "saved" = @Translation("Application code verified."),
 *    "skipped" = @Translation("Application codes not enabled.")
 *   }
 * )
 */
class TfaVaultTotpSetup extends TfaVaultTotpValidation implements TfaSetupInterface {

  /**
   * @var string
   */
  protected string $siteName;

  /**
   * Constructs a new Tfa plugin object.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin id.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\user\UserDataInterface $user_data
   *   User data object to store user specific information.
   * @param \Drupal\encrypt\EncryptionProfileManagerInterface $encryption_profile_manager
   *   Encryption profile manager.
   * @param \Drupal\encrypt\EncryptServiceInterface $encrypt_service
   *   Encryption service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The 'config.factory' service.
   * @param \Drupal\vault\VaultClientInterface $vault_client
   *   A vault client.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity_type.manager service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, UserDataInterface $user_data, EncryptionProfileManagerInterface $encryption_profile_manager, EncryptServiceInterface $encrypt_service, TranslationInterface $translation, ConfigFactoryInterface $config_factory, VaultClientInterface $vault_client, protected EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $user_data, $encryption_profile_manager, $encrypt_service, $translation, $config_factory, $vault_client);
    $site_name = $config_factory->get('system.site')->get('name');
    if(!is_string($site_name)) {
      $site_name = 'Drupal Site';
    }
    $this->siteName = $site_name;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('user.data'),
      $container->get('encrypt.encryption_profile.manager'),
      $container->get('encryption'),
      $container->get('string_translation'),
      $container->get('config.factory'),
      $container->get('vault.vault_client'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getSetupForm(array $form, FormStateInterface $form_state): array {
    $help_links = $this->getHelpLinks();

    $token_data = $this->createToken();

    if ($token_data == NULL) {
      // If we can't generate a token we can't proceed.
      return [];
    }

    $items = [];
    foreach ($help_links as $item => $link) {
      $items[] = Link::fromTextAndUrl($item, Url::fromUri($link, ['attributes' => ['target' => '_blank']]));
    }

    $form['apps'] = [
      '#theme' => 'item_list',
      '#items' => $items,
      '#title' => $this->t('Install authentication code application on your mobile or desktop device:'),
    ];
    $form['info'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t('The two-factor authentication application will be used during this setup and for generating codes during regular authentication. If the application supports it, scan the QR code below to get the setup code otherwise you can manually enter the text code.'),
    ];
    $form['seed'] = [
      '#type' => 'textfield',
      '#value' => $token_data['secret'],
      '#disabled' => TRUE,
      '#description' => $this->t('Enter this code into your two-factor authentication app or scan the QR code below.'),
    ];

    $form['token_data'] = [
      '#type' => 'item',
      '#description' => $this->t(
        'Algorithm: @algo; Period: @period; Digits: @digits;',
        [
          '@algo' => $this->tokenAlgorithm,
          '@period' => $this->tokenPeriod,
          '@digits' => $this->codeLength,
        ]
      ),
    ];

    // QR image of seed.
    $form['qr_image'] = [
      '#prefix' => '<div class="tfa-qr-code"',
      '#theme' => 'image',
      '#uri' => 'data:image/png;base64,' . $token_data['barcode'],
      '#alt' => $this->t('QR code for TFA setup'),
      '#suffix' => '</div>',
    ];

    // QR code css giving it a fixed width.
    $form['page']['#attached']['html_head'][] = [
      [
        '#tag' => 'style',
        '#value' => ".tfa-qr-code { width:200px }",
      ],
      'qrcode-css',
    ];

    // Include code entry form.
    $form = $this->getForm($form, $form_state);
    $form['actions']['login']['#value'] = $this->t('Verify and save');
    // Alter code description.
    $form['code']['#description'] = $this->t('A verification code will be generated after you scan the above QR code or manually enter the setup code. The verification code is six digits long.');
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateSetupForm(array $form, FormStateInterface $form_state): bool {
    $code = $form_state->getValue('code');
    if (!is_string($code) || !$this->validate($code)) {
      $this->errorMessages['code'] = $this->t('Invalid application code. Please try again.');
      return FALSE;
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function submitSetupForm(array $form, FormStateInterface $form_state): bool {
    // No operations required during save.
    return TRUE;
  }

  /**
   * Create token for account.
   *
   * @return array|null
   *
   * @phpstan-return array{'barcode': string, 'url': string, 'secret': string}|null
   */
  protected function createToken(): ?array {

    $generate_request = [
      'generate' => TRUE,
      'exported' => TRUE,
      'key_size' => $this->keySize,
      'issuer' => $this->issuer,
      'account_name' => $this->accountName(),
      'period' => $this->tokenPeriod,
      'algorithm' => $this->tokenAlgorithm,
      'digits' => $this->codeLength,
      'skew' => $this->timeSkew,
      'qr_size' => $this->qrSize,
    ];

    $vault_path = sprintf('/%s/keys/%s', $this->tokenPath, $this->getTokenName());

    try {
      $response = $this->vaultClient->write($vault_path, $generate_request);
    }
    catch (RequestException) {
      return NULL;
    }

    if ($response->getData() == NULL) {
      return NULL;
    }

    /** @var array{'barcode': string, 'url': string} $data */
    $data = $response->getData();

    /** @var array{'path': string, 'query': array{'algorithm': string, 'digits': string, 'issuer': string, 'period': string, 'secret': string}, 'fragment': string} $token_parsed */
    $token_parsed = UrlHelper::parse($data['url']);

    return [
      'barcode' => $data['barcode'],
      'url' => $data['url'],
      'secret' => $token_parsed['query']['secret'],
    ];
  }

  /**
   * Get account name for token generation.
   *
   * @return string
   *   URL encoded string.
   */
  protected function accountName(): string {
    $storage = $this->entityTypeManager->getStorage('user');
    /** @var \Drupal\user\Entity\User $account */
    $account = $storage->load($this->configuration['uid']);
    $prefix = $this->siteNamePrefix ? preg_replace('@[^a-z0-9-]+@', '-', $this->siteName) : $this->namePrefix;
    return urlencode((!empty($prefix) ? $prefix . '-' : '') . $account->getAccountName());
  }

  /**
   * {@inheritdoc}
   */
  public function getOverview(array $params): array {
    $plugin_text = $this->t('Validation Plugin: @plugin',
      [
        '@plugin' => str_replace(' Setup', '', $this->getLabel()),
      ]
    );
    $output = [
      'heading' => [
        '#type' => 'html_tag',
        '#tag' => 'h2',
        '#value' => $this->t('TFA application'),
      ],
      'validation_plugin' => [
        '#type' => 'markup',
        '#markup' => '<p>' . $plugin_text . '</p>',
      ],
      'description' => [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $this->t('Generate verification codes from a mobile or desktop application.'),
      ],
      'link' => [
        '#theme' => 'links',
        '#links' => [
          'admin' => [
            'title' => !$params['enabled'] ? $this->t('Set up application') : $this->t('Reset application'),
            'url' => Url::fromRoute('tfa.validation.setup', [
              'user' => $params['account']->id(),
              'method' => $params['plugin_id'],
            ]),
          ],
        ],
      ],
    ];
    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function getHelpLinks() {
    return $this->pluginDefinition['helpLinks'];
  }

  /**
   * {@inheritdoc}
   */
  public function getSetupMessages() {
    return ($this->pluginDefinition['setupMessages']) ?: '';
  }

}
