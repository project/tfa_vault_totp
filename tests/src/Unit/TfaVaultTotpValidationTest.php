<?php

namespace Drupal\Tests\tfa_vault_totp\Unit;

use PHPUnit\Framework\MockObject\MockObject;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Form\FormState;
use Drupal\encrypt\EncryptionProfileManagerInterface;
use Drupal\encrypt\EncryptServiceInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\tfa\Plugin\TfaValidationInterface;
use Drupal\tfa_vault_totp\Plugin\TfaValidation\TfaVaultTotpValidation;
use Drupal\user\UserDataInterface;
use Drupal\vault\VaultClientInterface;
use Vault\Exceptions\RequestException;
use Vault\ResponseModels\Response;

/**
 * Tests the the TFA Vault Validation plugin.
 *
 * @group tfa_vault_totp
 *
 * @covers \Drupal\tfa_vault_totp\Plugin\TfaValidation\TfaVaultTotpValidation
 * @codeCoverageIgnore
 */
class TfaVaultTotpValidationTest extends UnitTestCase {

  /**
   * Mock 'user.data' service.
   *
   * @var \Drupal\user\UserDataInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected MockObject|UserDataInterface $userDataMock;

  /**
   * Mock 'encrypt.encryption_profile.manager' service.
   *
   * @var \Drupal\encrypt\EncryptionProfileManagerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected EncryptionProfileManagerInterface|MockObject $encryptionProfileManagerMock;

  /**
   * Mock 'encryption' service.
   *
   * @var \Drupal\encrypt\EncryptServiceInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected MockObject|EncryptServiceInterface $encryptionServiceMock;

  /**
   * Mock 'vault.vault_client' service.
   *
   * @var \Drupal\vault\VaultClientInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected VaultClientInterface|MockObject $vaultClientMock;

  /**
   * Config for use with configuration service.
   *
   * @var array
   */
  protected array $config;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->userDataMock = $this->createMock(UserDataInterface::class);
    $this->encryptionProfileManagerMock = $this->createMock(EncryptionProfileManagerInterface::class);
    $this->encryptionServiceMock = $this->createMock(EncryptServiceInterface::class);
    $this->vaultClientMock = $this->createMock(VaultClientInterface::class);

    // TfaBasePlugin needs a mocked container.
    \Drupal::unsetContainer();
    $container = new ContainerBuilder();
    $container->set('config.factory', $this->getConfigFactoryStub(['tfa.settings' => ['encryption' => 'TEST_PROFILE']]));
    \Drupal::setContainer($container);

    $this->config = [
      'tfa.settings' => [
        'encryption' => '',
        'validation_plugin_settings' => [
          'tfa_vault_totp' => [
            'token_path' => 'Engine',
          ],
        ],
      ],
    ];
  }

  /**
   * Test the create method.
   */
  public function testCreate(): void {

    \Drupal::unsetContainer();
    $container = new ContainerBuilder();
    $container->set('user.data', $this->userDataMock);
    $container->set('encrypt.encryption_profile.manager', $this->encryptionProfileManagerMock);
    $container->set('encryption', $this->encryptionServiceMock);
    $container->set('string_translation', $this->getStringTranslationStub());
    $container->set('config.factory', $this->getConfigFactoryStub(['tfa.settings' => ['encryption' => '']]));
    $container->set('vault.vault_client', $this->vaultClientMock);
    \Drupal::setContainer($container);
    $plugin = TfaVaultTotpValidation::create($container, ['uid' => 1], 'tfa_vault_totp', []);
    $this->assertInstanceOf(TfaValidationInterface::class, $plugin);
  }

  /**
   *
   */
  public function testReady(): void {

    $this->vaultClientMock
      ->expects($this->exactly(3))
      ->method('read')
      ->with('/Engine/keys/drupal-vault-totp-1')
      ->willReturnOnConsecutiveCalls(
        new Response(['data' => ['valid' => TRUE]]),
        $this->throwException(new RequestException('', 404)),
        $this->throwException(new RequestException('', 500))
      );

    $plugin = new TfaVaultTotpValidation(['uid' => 1], 'tfa_vault_totp', [], $this->userDataMock, $this->encryptionProfileManagerMock, $this->encryptionServiceMock, $this->getStringTranslationStub(), $this->getConfigFactoryStub($this->config), $this->vaultClientMock);
    $this->assertTrue($plugin->ready());
    $this->assertFalse($plugin->ready());
    $this->expectException(\Exception::class);
    $this->expectExceptionMessage('Unknown fault validating provisioning state');
    $plugin->ready();
    // The exception above should stop test execution.
    $this->fail('failed to throw exception on invalid response in ready()');
  }

  /**
   * Test the code validation form.
   */
  public function testGetForm(): void {

    $plugin = new TfaVaultTotpValidation(['uid' => 1], 'tfa_vault_totp', [], $this->userDataMock, $this->encryptionProfileManagerMock, $this->encryptionServiceMock, $this->getStringTranslationStub(), $this->getConfigFactoryStub($this->config), $this->vaultClientMock);
    $form_state = new FormState();

    $form = $plugin->getForm([], $form_state);
    $this->assertIsArray($form, 'Code entry form returns an array');
    $this->assertArrayHasKey('code', $form, 'Code form must have code field');
    $this->assertStringContainsString('Verification code is application generated and 6 digits long.', $form['code']['#description']);
    $this->assertStringNotContainsString('Use one of your recovery codes.', $form['code']['#description']);

    // Check that notice is present for recovery code.
    $this->userDataMock
      ->method('get')
      ->with('tfa', '1', 'tfa_recovery_code')
      ->willReturn(['abcdef']);

    $form = $plugin->getForm([], $form_state);
    $this->assertIsArray($form, 'Code entry form returns an array');
    $this->assertArrayHasKey('code', $form, 'Code form must have code field');
    $this->assertStringContainsString('Verification code is application generated and 6 digits long.', $form['code']['#description']);
    $this->assertStringContainsString('Use one of your recovery codes.', $form['code']['#description']);
  }

  /**
   * Test the code validation form.
   */
  public function testBuildConfigurationForm(): void {

    // Override all config options so we can validate they change on form.
    $expected_values = [
      'time_skew' => 0,
      'site_name_prefix' => FALSE,
      'name_prefix' => 'SAMPLE_PREFIX',
      'issuer' => 'SAMPLE_ISSUER',
      'token_path' => 'Engine',
      'key_size' => 32,
      'period' => 60,
      'algorithm' => "SHA512",
      'code_length' => 8,
    ];

    $config = [
      'tfa.settings' => [
        'encryption' => '',
        'validation_plugin_settings' => [
          'tfa_vault_totp' => $expected_values,
        ],
      ],
    ];

    $this->vaultClientMock
      ->method('listSecretEngineMounts')
      ->with(['totp'])
      ->willReturn(['Engine/' => 'Sample mount 1', 'drupalTotp/' => 'Sample mount 2']);

    $plugin = new TfaVaultTotpValidation(['uid' => 1], 'tfa_vault_totp', [], $this->userDataMock, $this->encryptionProfileManagerMock, $this->encryptionServiceMock, $this->getStringTranslationStub(), $this->getConfigFactoryStub($config), $this->vaultClientMock);
    $form_state = new FormState();

    $form = $plugin->buildConfigurationForm($this->getConfigFactoryStub($config)->get('tfa.settings'), []);
    $this->assertIsArray($form, 'Code entry form returns an array');

    foreach ($expected_values as $key => $value) {
      $this->assertArrayHasKey($key, $form);
      $this->assertEquals($value, $form[$key]['#default_value'], "Form field for $key has $value for default value");
    }

    $this->assertEquals(['Engine' => 'Engine/', 'drupalTotp' => 'drupalTotp/'], $form['token_path']['#options']);

  }

  /**
   * Tests validateRequest() and validate().
   */
  public function testValidateForm(): void {

    $this->vaultClientMock
      ->expects($this->exactly(5))
      ->method('write')
      ->with(
        '/Engine/code/drupal-vault-totp-1',
        ['code' => 123456]
      )
      ->willReturnOnConsecutiveCalls(
        new Response(['data' => ['valid' => TRUE]]),
        new Response(['data' => ['valid' => FALSE]]),
        // Response, but missing data.
        new Response(),
        // Code already accepted.
        $this->throwException(new RequestException('', 400)),
        // Unknown error.
        $this->throwException(new RequestException('', 500))
      );

    $plugin = new TfaVaultTotpValidation(['uid' => 1], 'tfa_vault_totp', [], $this->userDataMock, $this->encryptionProfileManagerMock, $this->encryptionServiceMock, $this->getStringTranslationStub(), $this->getConfigFactoryStub($this->config), $this->vaultClientMock);

    $form_state = new FormState();
    $form = [];
    $form_state->setValue('code', 123456);

    // Valid token.
    $this->assertTrue($plugin->validateForm($form, $form_state));
    $this->assertEmpty($plugin->getErrorMessages(), "No errors reported");

    // Invalid token.
    $this->assertFalse($plugin->validateForm($form, $form_state));
    $this->assertNotEmpty($plugin->getErrorMessages(), "No errors reported");
    $this->assertArrayHasKey('code', $plugin->getErrorMessages());
    $this->assertEquals('Invalid application code. Please try again.', $plugin->getErrorMessages()['code'], 'Code invalid message present');

    // No data in response from vault.
    $this->assertFalse($plugin->validateForm($form, $form_state));
    $this->assertNotEmpty($plugin->getErrorMessages(), 'Errors reported');
    $this->assertArrayHasKey('code', $plugin->getErrorMessages());
    $this->assertEquals('Invalid application code. Please try again.', $plugin->getErrorMessages()['code'], 'Code invalid message present');

    // Code previously accepted.
    $this->assertFalse($plugin->validateForm($form, $form_state));
    $this->assertNotEmpty($plugin->getErrorMessages(), 'Errors reported');
    $this->assertArrayHasKey('code', $plugin->getErrorMessages());
    $this->assertEquals('Invalid code, it was recently used for a login. Please try a new code.', $plugin->getErrorMessages()['code'], 'Code previously used message present');

    // Unexpected server error.
    $this->assertFalse($plugin->validateForm($form, $form_state));
    $this->assertNotEmpty($plugin->getErrorMessages(), 'Errors reported');
    $this->assertArrayHasKey('code', $plugin->getErrorMessages());
    $this->assertEquals('Invalid application code. Please try again.', $plugin->getErrorMessages()['code'], 'Code invalid message present');
  }

  /**
   * Tests validateRequest() and isAlreadyAccepted().
   *
   * By proxy also tests validate().
   */
  public function testValidateRequest(): void {

    $this->vaultClientMock
      ->expects($this->exactly(5))
      ->method('write')
      ->with(
        '/Engine/code/drupal-vault-totp-1',
        ['code' => 123456]
      )
      ->willReturnOnConsecutiveCalls(
        new Response(['data' => ['valid' => TRUE]]),
        new Response(['data' => ['valid' => FALSE]]),
        // Response, but missing data.
        new Response(),
        // Code already accepted.
        $this->throwException(new RequestException('', 400)),
        // Unknown error.
        $this->throwException(new RequestException('', 500))
      );

    $plugin = new TfaVaultTotpValidation(['uid' => 1], 'tfa_vault_totp', [], $this->userDataMock, $this->encryptionProfileManagerMock, $this->encryptionServiceMock, $this->getStringTranslationStub(), $this->getConfigFactoryStub($this->config), $this->vaultClientMock);
    $this->assertTrue($plugin->validateRequest(123456));
    $this->assertFalse($plugin->isAlreadyAccepted(), 'Code was not previously accepted');
    $this->assertFalse($plugin->validateRequest(123456));
    $this->assertFalse($plugin->isAlreadyAccepted(), 'Code was not previously accepted');
    $this->assertFalse($plugin->validateRequest(123456));
    $this->assertFalse($plugin->isAlreadyAccepted(), 'Code was not previously accepted');
    $this->assertFalse($plugin->validateRequest(123456));
    $this->assertTrue($plugin->isAlreadyAccepted(), 'Code was previously accepted');
    $this->assertFalse($plugin->validateRequest(123456));
    $this->assertFalse($plugin->isAlreadyAccepted(), 'Code was not previously accepted');

  }

}
