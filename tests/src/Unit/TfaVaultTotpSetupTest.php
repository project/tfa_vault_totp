<?php

namespace Drupal\Tests\tfa_vault_totp\Unit;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormState;
use Drupal\encrypt\EncryptionProfileManagerInterface;
use Drupal\encrypt\EncryptServiceInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\tfa\Plugin\TfaSetupInterface;
use Drupal\tfa_vault_totp\Plugin\TfaSetup\TfaVaultTotpSetup;
use Drupal\user\Entity\User;
use Drupal\user\UserDataInterface;
use Drupal\user\UserStorageInterface;
use Drupal\vault\VaultClientInterface;
use PHPUnit\Framework\MockObject\MockObject;
use Vault\Exceptions\RequestException;
use Vault\ResponseModels\Response;

/**
 * Tests the TFA Vault Setup plugin.
 *
 * @group tfa_vault_totp
 *
 * @covers \Drupal\tfa_vault_totp\Plugin\TfaSetup\TfaVaultTotpSetup
 * @codeCoverageIgnore
 */
class TfaVaultTotpSetupTest extends UnitTestCase {

  /**
   * Mock 'user.data' service.
   *
   * @var \Drupal\user\UserDataInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected MockObject|UserDataInterface $userDataMock;

  /**
   * Mock 'encrypt.encryption_profile.manager' service.
   *
   * @var \Drupal\encrypt\EncryptionProfileManagerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected EncryptionProfileManagerInterface|MockObject $encryptionProfileManagerMock;

  /**
   * Mock 'encryption' service.
   *
   * @var \Drupal\encrypt\EncryptServiceInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected MockObject|EncryptServiceInterface $encryptionServiceMock;

  /**
   * Mock 'vault.vault_client' service.
   *
   * @var \Drupal\vault\VaultClientInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected VaultClientInterface|MockObject $vaultClientMock;

  /**
   * Plugin Definition.
   *
   * @var array
   */
  protected array $pluginDefinition;

  /**
   * Common config for ConfigFactory stub.
   *
   * @var array
   */
  protected array $config;

  /**
   *  Mock 'entity_type.manager' service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected EntityTypeManagerInterface|MockObject $entityTypeManagerMock;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->userDataMock = $this->createMock(UserDataInterface::class);
    $this->encryptionProfileManagerMock = $this->createMock(EncryptionProfileManagerInterface::class);
    $this->encryptionServiceMock = $this->createMock(EncryptServiceInterface::class);
    $this->vaultClientMock = $this->createMock(VaultClientInterface::class);

    // TfaBasePlugin needs a mocked container.
    \Drupal::unsetContainer();
    $container = new ContainerBuilder();
    $container->set('config.factory', $this->getConfigFactoryStub(['tfa.settings' => ['encryption' => 'TEST_PROFILE']]));
    \Drupal::setContainer($container);

    $this->pluginDefinition = [
      'id' => 'tfa_vault_totp_setup',
      'label' => 'TFA Vault TOTP Setup',
      'description' => 'TFA Vault TOTP Setup Plugin',
      'helpLinks' => [
        'Google Authenticator (Android/iOS)' => 'https://googleauthenticator.net',
        'Microsoft Authenticator (Android/iOS)' => 'https://www.microsoft.com/en-us/security/mobile-authenticator-app',
      ],
      'setupMessages' => [
        "saved" => "Application code verified.",
        "skipped" => "Application codes not enabled.",
      ],
    ];

    $totp_config_values = [
      'time_skew' => 0,
      'site_name_prefix' => FALSE,
      'name_prefix' => 'SAMPLE_PREFIX',
      'issuer' => 'SAMPLE_ISSUER',
      'token_path' => 'Engine',
      'key_size' => 32,
      'period' => 60,
      'algorithm' => "SHA512",
      'code_length' => 8,
    ];

    $this->config = [
      'tfa.settings' => [
        'encryption' => '',
        'validation_plugin_settings' => [
          'tfa_vault_totp' => $totp_config_values,
        ],
      ],
      'system.site' => ['name' => 'Unit Test Site'],
    ];

    $user_mock = $this->createMock(User::class);
    $user_mock->method('getAccountName')->willReturn('AdminUser');
    $user_storage_mock = $this->createMock(UserStorageInterface::class);
    $user_storage_mock
      ->method('load')
      ->with('1')
      ->willReturn($user_mock);

    $this->entityTypeManagerMock = $this->createMock(EntityTypeManagerInterface::class);
    $this->entityTypeManagerMock
      ->method('getStorage')
      ->with('user')
      ->willReturn($user_storage_mock);

  }

  /**
   * Test the create method.
   */
  public function testCreate(): void {

    \Drupal::unsetContainer();
    $container = new ContainerBuilder();
    $container->set('user.data', $this->userDataMock);
    $container->set('encrypt.encryption_profile.manager', $this->encryptionProfileManagerMock);
    $container->set('encryption', $this->encryptionServiceMock);
    $container->set('string_translation', $this->getStringTranslationStub());
    $container->set('config.factory', $this->getConfigFactoryStub(
      [
        'tfa.settings' => ['encryption' => 'UnitTestEncryptionProfile'],
        'system.site' => ['name' => 'Unit Test Site'],
      ]
    ));
    $container->set('entity_type.manager', $this->entityTypeManagerMock);
    $container->set('vault.vault_client', $this->vaultClientMock);
    \Drupal::setContainer($container);
    $plugin = TfaVaultTotpSetup::create($container, ['uid' => 1], 'tfa_vault_totp', []);
    $this->assertInstanceOf(TfaSetupInterface::class, $plugin);
  }

  /**
   * Test getSetupForm() generation.
   */
  public function testGetSetupForm(): void {
    $response_data = [
      'barcode' => 'iVBORw0KGgoAAAANSUhEUgAAAMgAAADIEAAAAADYoy0BAAAG3ElEQVR4nOydwY7kOAxDtxf9/7/ce/DFC4EcSkowrAbfqZFybGcIaWRJlfr++fknGPHv395A+D8RxIwIYkYEMSOCmBFBzIggZkQQMyKIGRHEjAhiRgQxI4KYEUHMiCBmRBAzIogZEcSMCGLGtzrw60sdear0Z/xdsb9nUK4r+6nj6z75nHx1/VOO/lyxEDMiiBmyyzro5s9nQCPR9bpunedcqde5q7zH89mQk9z/m9zEQsyIIGY0XdZBiWRQHFUdBRqPnI/iNJAbQTPcd6Gn486nG90hYiFmRBAzRi5L4XYO3BXc4+t1ZXxdEf3N96k8izJ+QyzEjAhixmsuSzkA8uPe7e44SiSm7FA/2L5HLMSMCGLGyGXpZjs77qFjIxp5o+ed+BG1m4N6ypXFQsyIIGY0XZZuyMoxTf9bX+vw1FqKg+06N04sxIwIYobssjZRBIqLeNvApoWAR3TKvZWnEuycWIgZEcSML9XsuscoHsPMUtmzeEbvttIjLt57xuuhnFiIGRHEjJHLqtcPyHjrSEQ319Q93PH9zFyr0m4al/WxRBAzHs1lKY0NB/Sp3nKAZpvFb+i4inaI7q1juofHWIgZEcSMFyqGSjK8zqNUA7k74vfeq6Ax6FM9/6a4a04sxIwIYoZ8MIQTvBDzdL/ZN4tz9HT6ZsUcDD+cCGJGM5d1UHJWs8zSLE+FdjsrDaCZu1/kmf1nEAsxI4KYMYqyZm6Ez/Bs91T36Fo/rTOjHNfGtVZiIWZEEDPW6Xe9r0nvnrrHK73x6Lpe+9MLB906ZpdYiBkRxIzFy2eU2OO+3j2g8Z5ztBMldlKiLPSpXgKYEQsxI4KYMcplHbotlJt8UV0Frat3Yd3z6+4X7XZfdDjEQsyIIGY0v7CDoiB05b7e7XdCzHJW3egLPe8sNtOJhZgRQcxY92V1K4b3DLM0tZIGv+ERoJ7Xqn+jnWxirViIGRHEjHWTAxpzw92CspZSPdRdxKzv69nSAyIWYkYEMePRJodNv5OeGVOqfvs+K71xQjn2xmV9LBHEjNEv7CDTVhLgihPoRnF8jF77050Vz9opqyNiIWZEEDMWFcMKz3HVefTmTLSHbjK8m1tDnyqHUL1McBMLMSOCmLF+XxZnlq3S96OjFwi6R060VlzWryCCmNHMZXUzSzf6gUuPYfTMWN3J5rneSLwfYiFmRBAzmrkspbfqHsMzXfo8PLbRIzQUa836qdC66KkVYiFmRBAzRt8xRM2WumHybJXurPi6fG/dqE9fF82vEAsxI4KY8dCvRW/63tHIihKhIZBrqhFRHTNzpF1ndYiFmBFBzBjlsvQENU9xK2ntbjeUnmGb1SVn6+rEQsyIIGY89CYH5HbeTlZ3c1zdI6QS0aGYbea4YiFmRBAzHmpy0HvX7zH8XnRFH9nNs3V3y2fQ172JhZgRQcwYvclB707nLqWbwEc1vllGi6+IjpCbcoBCLMSMCGLG+n1ZPHPFc1ab3nXduW06x/in+hWdWIgZEcSMxfuy6nVeSdz3tysZM16RvOfRSwCcWbyHiIWYEUHMGKXfFUc0izS4A9m7lLoKWldpt+DX00r6K4ggZix+x7B7JKx33exbHbhD02MhPfmvkIPhhxNBzFhXDJV8ETqCdXuolL2hT2+edV98rToPJxZiRgQxY1ExrPkrpRFi40D0htWNY9Ejt9nzcmIhZkQQM0av+Lvp9juhu3gMVu9S+r5Qml1ZF+1Wd3foSTmxEDMiiBmLL+zwOEdBj082LaNvOEw0s7J/TizEjAhixijKQsdDva9JiUPQ8VOv6ynPUhtT+U7QnvcdWYdYiBkRxIzFL+zMKnRKJbGbotevo+fiDQ/8GfkMXWIhZkQQM0bp95lJokwUz1Chv7m76x7i+Bh011OdYzexEDMiiBnrviylIbOb89FjG0432f5U1S+5rF9EBDFj4bL+MPGoCVM5as1S4mg8n1/ZW51hQyzEjAhixrrJoaK7mm4LKNoDT+nre9AdI1q37jNNDh9OBDFj8Yq/Cm8n4FU2JcJBayn1Pr6rblaNr7vJ+8VCzIggZjz07nfdHentBMhpKAlztK7SCcYdF18X7VMnFmJGBDFj5LIUuPnz60oHF3c+6Iri9PRjZr1LmY0TCzEjgpjxmss67LvKeZzGj29KlKUn5Hn7BD8k6sRCzIggZoxclmKGensAz2XxaIfvR+/FUo6NiFlxARELMSOCmLF4kTJCN+Fue+csCa+4IH2Mfkjku0LEQsyIIGa81pcVZsRCzIggZkQQMyKIGRHEjAhiRgQxI4KYEUHMiCBmRBAzIogZEcSMCGJGBDEjgpgRQcyIIGZEEDMiiBn/BQAA//+1I2GwpophHwAAAABJRU5ErkJggg==',
      'url' => 'otpauth://totp/SAMPLE_ISSUER:SAMPLE_PREFIX-AdminUser?algorithm=SHA512&digits=8&issuer=SAMPLE_ISSUER&period=60&secret=NZRQ73GBIUAUU53CSWB2UFCMECANIRJZ2LNG4JCXUYKNE7SSRQRQ',
    ];

    $expected_request_params = [
      'generate' => TRUE,
      'exported' => TRUE,
      'key_size' => 32,
      'issuer' => 'SAMPLE_ISSUER',
      'account_name' => 'SAMPLE_PREFIX-AdminUser',
      'period' => 60,
      'algorithm' => 'SHA512',
      'digits' => 8,
      'skew' => 0,
      'qr_size' => 200,
    ];

    $this->vaultClientMock
      ->method('write')
      ->with('/Engine/keys/drupal-vault-totp-1', $expected_request_params)
      ->willReturnOnConsecutiveCalls(
        new Response(['data' => $response_data]),
        // Response, but missing data.
        new Response(),
        // Unknown error.
        $this->throwException(new RequestException('', 500))
      );
    $plugin = new TfaVaultTotpSetup(['uid' => 1], 'tfa_vault_totp', $this->pluginDefinition, $this->userDataMock, $this->encryptionProfileManagerMock, $this->encryptionServiceMock, $this->getStringTranslationStub(), $this->getConfigFactoryStub($this->config), $this->vaultClientMock, $this->entityTypeManagerMock);
    $form_state = new FormState();
    $result = $plugin->getSetupForm([], $form_state);
    $this->assertIsArray($result);
    $this->assertNotEmpty($result, 'getSetupForm() not empty');
    $this->assertEquals('NZRQ73GBIUAUU53CSWB2UFCMECANIRJZ2LNG4JCXUYKNE7SSRQRQ', $result['seed']['#value'], 'Token field populated');
    $this->assertEquals('data:image/png;base64,' . $response_data['barcode'], $result['qr_image']['#uri'], 'Barcode is correctly displayed');

    $this->assertEquals([], $plugin->getSetupForm([], $form_state), 'No form when vault returns no data');
    $this->assertEquals([], $plugin->getSetupForm([], $form_state), 'No form when vault returns an exception');
  }

  /**
   * Test validateSetupForm();
   */
  public function testValidateSetupForm(): void {

    $this->vaultClientMock
      ->expects($this->exactly(2))
      ->method('write')
      ->with(
        '/Engine/code/drupal-vault-totp-1',
        ['code' => 123456]
      )
      ->willReturnOnConsecutiveCalls(
        new Response(['data' => ['valid' => TRUE]]),
        new Response(['data' => ['valid' => FALSE]]),
      );

    $plugin = new TfaVaultTotpSetup(['uid' => 1], 'tfa_vault_totp', $this->pluginDefinition, $this->userDataMock, $this->encryptionProfileManagerMock, $this->encryptionServiceMock, $this->getStringTranslationStub(), $this->getConfigFactoryStub($this->config), $this->vaultClientMock, $this->entityTypeManagerMock);
    $form_state = new FormState();
    $form_state->setValue('code', '123456');
    $this->assertTrue($plugin->validateSetupForm([], $form_state));
    $this->assertEmpty($plugin->getErrorMessages());
    $this->assertFalse($plugin->validateSetupForm([], $form_state));
    $this->assertNotEmpty($plugin->getErrorMessages());
    $this->assertEquals('Invalid application code. Please try again.', $plugin->getErrorMessages()['code'], 'Error message is present');
  }

  /**
   * Tests submitSetupForm().
   */
  public function testSubmitSetupForm(): void {
    $plugin = new TfaVaultTotpSetup(['uid' => 1], 'tfa_vault_totp', $this->pluginDefinition, $this->userDataMock, $this->encryptionProfileManagerMock, $this->encryptionServiceMock, $this->getStringTranslationStub(), $this->getConfigFactoryStub($this->config), $this->vaultClientMock, $this->entityTypeManagerMock);
    $form_state = new FormState();
    $this->assertTrue($plugin->submitSetupForm([], $form_state), 'submitSetupForm() always returns true');
  }

  /**
   * Tests submitSetupForm().
   */
  public function testGetOverview(): void {
    $plugin = new TfaVaultTotpSetup(['uid' => 1], 'tfa_vault_totp', $this->pluginDefinition, $this->userDataMock, $this->encryptionProfileManagerMock, $this->encryptionServiceMock, $this->getStringTranslationStub(), $this->getConfigFactoryStub($this->config), $this->vaultClientMock, $this->entityTypeManagerMock);

    $user_mock = $this->createMock(User::class);
    $user_mock->method('id')->willReturn(1);
    $params = [
      'enabled' => FALSE,
      'account' => $user_mock,
      'plugin_id' => 'tfa_vault_totp',
    ];
    $result = $plugin->getOverview($params);
    $this->assertIsArray($result);
    $this->assertStringContainsString('Set up application', $result['link']['#links']['admin']['title'], 'Link text includes setup when plugin not enabled');

    // Test when plugin is configured already.
    $params['enabled'] = TRUE;
    $result = $plugin->getOverview($params);
    $this->assertIsArray($result);
    $this->assertStringContainsString('Reset application', $result['link']['#links']['admin']['title'], 'Link text includes setup when plugin not enabled');

  }

  /**
   * Tests getHelpLinks().
   */
  public function testGetHelpLinks(): void {

    $sample_help_links = [
      'Google Authenticator (Android/iOS)' => 'https://googleauthenticator.net',
      'Microsoft Authenticator (Android/iOS)' => 'https://www.microsoft.com/en-us/security/mobile-authenticator-app',
    ];

    $plugin = new TfaVaultTotpSetup(['uid' => 1], 'tfa_vault_totp', $this->pluginDefinition, $this->userDataMock, $this->encryptionProfileManagerMock, $this->encryptionServiceMock, $this->getStringTranslationStub(), $this->getConfigFactoryStub($this->config), $this->vaultClientMock, $this->entityTypeManagerMock);
    $this->assertEquals($sample_help_links, $plugin->getHelpLinks(), 'Help links returned');
  }

  /**
   *
   */
  public function testGetSetupMessages(): void {

    $sample_setup_messages = [
      "saved" => "Application code verified.",
      "skipped" => "Application codes not enabled.",
    ];

    $plugin = new TfaVaultTotpSetup(['uid' => 1], 'tfa_vault_totp', $this->pluginDefinition, $this->userDataMock, $this->encryptionProfileManagerMock, $this->encryptionServiceMock, $this->getStringTranslationStub(), $this->getConfigFactoryStub($this->config), $this->vaultClientMock, $this->entityTypeManagerMock);
    $this->assertEquals($sample_setup_messages, $plugin->getSetupMessages(), 'Setup messages returned');
  }

}
